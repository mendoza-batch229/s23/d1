// console.log("Hello AL");

// [SECTION] -Objects



/*
SYNTAX -> let/const objectName={
	keyA: valueA,
	keyB: vaueB	
}
*/
// Object Literal
// {} = object literal Great for creating dynamic object/pag kailangan lang
let cellphone={
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log("Result from creating objects using initializers/literal notations example");
console.log(cellphone);
console.log(typeof cellphone);




// Constructor Function
// COnstructor function - allows us to create objects with a constructor object
// This is an object
// (this) use create new property
function Laptop(name, manufactureDate){
	this.name = name,
	this.manufactureDate = manufactureDate
};

// This is an instance of an object
// instance = bagong property value (new)
// we cannot create  new object without our "instance" or the (new) keyword.
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constructor example1");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating object using object constructor example2");
console.log(myLaptop);
console.log(typeof myLaptop);

let oldLaptop = new Laptop("Portal r2E CCMC", 1980);
console.log("Result from creating object using object constructor example3");
console.log(oldLaptop);
console.log(typeof oldLaptop);



// Creating empty objects
let computer = {}
console.log(computer);

let array = [laptop,myLaptop];

// bracket notation  not recomended
console.log(array[0]["name"]);

// recomended using dot notation
console.log(array[0].name);



// [SECTION] -> Initialzing, Deleteng, Re-assigning/Updating Object Properties

let car = {};

// Adding key pairs
car.brand = "Honda";
car.model = "Honda Civic"
car.price = 1368000
console.log(car);

// Deleting properties
delete car.price;
console.log(car);

// Re-assigning/Updating
car.model = "Honda Vios";
console.log(car);



// Objects can have arrays as properties
let professor1 = {
	name: "ROmenick Garcia",
	age:  25,
	subjects: ["MongoDB", "HTML", "CSS", "Js"]
};
// Access object properties(subject) using dot notation
console.log(professor1.subjects);

// What if we want to add  items in subject array within the object
professor1.subjects.push("NodeJS");
console.log(professor1.subjects);


// Display each subject from professor1's subject array:
// approach 1 - access the items using the array index
console.log("Result from creating object using approach 1");
console.log(professor1.subjects[0]); 
console.log(professor1.subjects[1]); 
console.log(professor1.subjects[2]); 
console.log(professor1.subjects[3]); 
console.log(professor1.subjects[4]); 

// approach 2 -we can also use  forEach() to display item in each array
console.log("Result from creating object using approach 2");
professor1.subjects.forEach(subject => console.log(subject));




let dog1 = {
	name: "Bantay",
	breed: "Golden Ret"
};
let dog2 = {
	name: "Bolt",
	breed: "Aspin"
};

// Arrays of object
let dogs =[dog1,dog2];

// How can we display the detals of the first in the dogd array?
console.log(dogs[0]);
// How can we display the value of the name property
console.log(dogs[1].name);

// Delete 
dogs.pop()


// Add
dogs.push({
	name: "Whitey",
	breed: "Shih Tzu"
});
// console.log(dogs);


// Object Methods
// Function in an object
// This is useful for creating functions that are associated to  a specific object

let person1 = {
	name: "Joji",
	talk: function(){
		console	.log("Hello!");
	}
}
person1.talk();


let person2 = {
	name: "Joe",
	talk: function(){
		console.log('Hello World!');
	}
}
person2.talk();

person2.walk = function(){
	console.log('Joe has walked 500 miles just to be the man that walked 1000 miles to be at your door.');
}
person2.walk();


// (this) keyword in an object method refers to the current object where method is.
person1.introduction = function(){
	console.log("Hi I am " + this.name + "!");
}
person1.introduction();

person2.introduction = function(){
	console.log(this);
}
person2.introduction();

// Mini Activity 
// Create a new object ad student1
// Should have the fllowing properties : name,age,address
// add 3 methods for student1
// intruduceName = display s1 name as "Hello! My name is <student1name>"
// introduceAge = s1 age as "Hello! I am <s1 age> years old."
// "Hello! I am <name s1>. I live in <address>."
// Invoke all 3 methods

// let student1 = {
// 	name: "Al Zailo",
// 	age: 22,
// 	address: "Cagayan"
// };
// console.log("Hello! My name is " + student1.name );
// console.log("Hello! I am " + student1.age + " years old")
// console.log("Hello! I Am " + student1.name + ", I live in " + student1.address);

function Student(name,age,address){
	this.name = name;
	this.age = age;
	this.address = address;

	// we can also add methods  to our constructor function
	this.introName = function(){
		console.log("Hello! My name is " + this.name);
	}

	this.introAge = function(){
		console.log("Hello! I am " + this.age + " years old");
	}

	this.introAdd = function(){
		console.log("Hello! I Am " + this.name + ", I live in " + this.address);
	}
	// we can also add methods that take other object as an argumensts
	this.greet = function(person){
		// person properties sare accessible in the method
		console.log("Good day " + person.name + "!");
	}

}
// console.log(Student);

let newStudent1 = new Student("TeeJae", 25, "Cainta,Rizal");
console.log(newStudent1);

let newStudent2 = new Student("Jeremiah", 26, "Lucena, Quezon");
newStudent2.greet(newStudent1);

// mini activity 
// create a new constructor function called dDog which able to join objects with the ff:
// name, breed
// should have 2 methods
// call() - is a method which allow to display the following message:
// "bark Bark Bark!"
// greet method allow us to greet another person

function Dog(name,breed){
	this.name = name;
	this.breed = breed;

	this.call = function(){
		console.log("Bark Bark Bark!");
	}

	this.greet = function(person1){
		// we assuming that the data as person is an object with a name property.
		console.log("Bark Bark " + person1.name + "!");
	}
}

let newDog5 = new Dog("Popol","Golden Ret");
let newDog = new Dog("Cobid","Golden Ret");
newDog.call();
newDog.greet(car);


// when creating a function that is supposed to recieved data/argument
function sample1(sampleParameter){
	console.log(sampleParameter.name);
	newDog.name = "Gary";
}
sample1(newDog);
// sample1(250000);
console.log(newDog);












